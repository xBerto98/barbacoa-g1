== Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Condimentos

* Limón
* Mantequilla
* Sal

=== Aperitivos

* Aceitunas
* Anacardos
* Frutos secos
* Kikos
* Patatas fritas
* Pistachos
* Salchichón

=== Platos principales

* Carrillá
* Chuletas de cabeza
* Costillas de cerdo
* Filetes de pollo
* Patatas para asar
* Verduras para asar

=== Postres

* Arroz con leche
* Flan
* Mandarinas
* Natillas
